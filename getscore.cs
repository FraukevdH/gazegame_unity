﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class getscore : MonoBehaviour {
	public static Text txt;
	public static int x = 0;


	// Use this for initialization
	void Start () {
		txt = GameObject.Find ("score").GetComponent<Text> ();	
	}
		
	// Update is called once per frame
	void Update () {
		if (x < 1000) {
			x += 1;
		} 
		txt.text = "Score:" + x;
	}
}
