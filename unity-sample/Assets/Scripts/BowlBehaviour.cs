﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

public enum BowlState
{
	full, 
	empty
};

public class BowlBehaviour : MonoBehaviour {
	public BowlState state;
	public SpriteRenderer spriteRenderer;
	public Sprite[] spriteArray;
	
	IEnumerator SetState(BowlState new_state, float wait_time)
	{
		yield return new WaitForSeconds(wait_time); //wait 10 seconds
		this.state = new_state;
	}

	// Use this for initialization
	void Start () {
		this.state = BowlState.full;
		spriteRenderer.sprite = spriteArray[1];
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (/*this.GetComponent<VRInteractiveItem>().IsOver &&*/ Input.GetKeyDown("space"))
        {
			if (this.state == BowlState.empty)
			{
				Fill_Bowl();
			}
        }
	}

public	void Fill_Bowl()
	{
		spriteRenderer.sprite = spriteArray[1];
		this.state = BowlState.full;
	}

	public void Empty_Bowl()
	{
		spriteRenderer.sprite = spriteArray[0];
		this.state = BowlState.empty;
	}
}
