﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VRStandardAssets.Utils;

[System.Serializable]
public class WaveAction
{
    public string name;
    public float delay;
    public Transform prefab;
    public int spawnCount;
    public string message;
}

[System.Serializable]
public class Wave
{
    public string name;
    public List<WaveAction> actions;
}



public class WaveManager : MonoBehaviour
{
    public float difficultyFactor = 0.9f;
    public List<Wave> waves;
    public Wave m_CurrentWave;
    private Wave CurrentWave { get { return m_CurrentWave; } }
    [SerializeField]  public float m_DelayFactor = 1.0f;
    public static System.Random ran = new System.Random();
    public int randomer;
    [SerializeField] public bool isGameActive = true;
    private float xSpawnRange = 7.5f;
    private float ySpawnRange = 4.5f;

    private int activeEnemyCount = 0;


    [SerializeField] public GameObject[] spwanpoints;


    IEnumerator SpawnLoop()
    {
     
            foreach (Wave W in waves)
            {
                m_CurrentWave = W;
                foreach (WaveAction A in W.actions)
                {
                    if (A.delay > 0)
                        yield return new WaitForSeconds(A.delay * m_DelayFactor);
                    if (A.message != "")
                    {
                        // TODO: print ingame message
                    }
                    if (A.prefab != null && A.spawnCount > 0)
                    {
                        for (int i = 0; i < A.spawnCount; i++)
                        {
                            StartCoroutine(SpawnRoutine(A));
                            yield return new WaitForSeconds(A.delay);
                        }
                    }
                }
                yield return null;  // prevents crash if all delays are 0
            }
            m_DelayFactor *= difficultyFactor;

    }
    void Start()
    {

  
        StartCoroutine(SpawnLoop());
    }

    public void DecrementEnemyCount()
    {
        if (activeEnemyCount > 0)
        {
            --activeEnemyCount;
        }
    }

    // Destructible.cs
    private void OnDestroy()
    {
        if (tag.Equals("Enemy"))
        {
            this.DecrementEnemyCount();
        }
    }

    //spawning using coroutine
    IEnumerator SpawnRoutine(WaveAction act)
    {
        if (act.prefab.gameObject.tag == "Enemy")
        {
            if (isGameActive)
            {
                //   int randomSpawn = (int)(Random.Range(0f, (float)enemyPrefabList.Length - 1));
                GameObject enemyPrefab = act.prefab.gameObject;
                float posy = Random.Range(-ySpawnRange, ySpawnRange);
                float[] randomx = { -xSpawnRange, xSpawnRange };
                int xindex = Random.Range(0, randomx.Length);
                float randomer_x = randomx[xindex];
                Vector3 transvec = new Vector3(randomer_x, posy, 0);
                GameObject enemyObject = (GameObject)Instantiate(enemyPrefab, this.transform.position, Quaternion.identity);
                enemyObject.transform.parent = this.transform;
                enemyObject.transform.localPosition = transvec;

                //add interaction functionality
                //VRInteractiveItem vrii = enemyObject.AddComponent<VRInteractiveItem>();

                activeEnemyCount++;
                yield return new WaitForSeconds(act.delay);

            }
        }


        if (act.prefab.gameObject.tag == "Friend")
        {
            int spawn_index = Random.Range(0, spwanpoints.Length - 1);
            Transform s_point = spwanpoints[spawn_index].transform;
            GameObject friendPrefab = act.prefab.gameObject;
            GameObject enemyObject = (GameObject)Instantiate(friendPrefab, s_point.position, Quaternion.identity);

            enemyObject.transform.parent = this.transform;
            yield return new WaitForSeconds(act.delay);
        }
    }
}