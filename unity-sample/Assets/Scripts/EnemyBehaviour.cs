﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

public enum EnemyState
{
	norm,
	leave, 
	attack, 
	celebrate, 
	die
};
public class EnemyBehaviour : MonoBehaviour {

	[SerializeField] public float speed = 0.5f;
	[SerializeField] public float randomer = 1f;
	[SerializeField] public float randomer1 = 10f;
	[SerializeField] public float randomer2 = 0.05f;
	[SerializeField] public float randomer3 = 0.0005f;

	public GameObject[] goal_points;
	private GameObject goal_point;
	private bool triggered = false;
	private GameManager theGM;
	private EnemyState state;
	private GameObject victim;

	// Use this for initialization
	void Start() 
	{
		theGM = FindObjectOfType<GameManager>();
		this.state = EnemyState.norm;
	 goal_points = GameObject.FindGameObjectsWithTag("EnemyGoalPoint");
		goal_point = goal_points[Random.Range(0, goal_points.Length)];	
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (triggered && !victim)
		{
			StartCoroutine(SetState(EnemyState.norm, 0f));
		}

        if (this.GetComponent<VRInteractiveItem>().IsOver)
        {
			this.state = EnemyState.die;
        }
		
		Vector3 pos = new Vector3();
		switch (state)
		{
		
			case EnemyState.norm:
			pos = new Vector3((Random.value - 0.5f) * randomer * speed,
			(Random.value - 0.5f) * randomer1 * speed, 0f);
		transform.position = Vector3.Lerp(transform.position,
			   transform.position + pos, randomer2);
		transform.position = Vector3.Lerp(transform.position,
			  goal_point.transform.position, randomer3);
		break;
			case EnemyState.leave:
				transform.position = Vector3.MoveTowards(transform.position, -goal_point.transform.position*3,0.05f);
				break;
			case EnemyState.attack:
				if (victim != null)
				{
					if (victim.GetComponent<FriendBehaviour>().state == FriendState.leave) {
						victim = null;
						SetState(EnemyState.norm, 0f);
					}
                    else
                    {
						transform.position = Vector3.MoveTowards(transform.position, victim.transform.position, 0.4f * speed);
					}
				}
                else
                {
					SetState(EnemyState.norm, 0f);
                }
				break;
			case EnemyState.celebrate:
				pos = new Vector3((Random.value - 0.5f) * randomer * speed *4,
		(Random.value - 0.5f) * randomer1 * speed *4 , 0f);
		transform.position = Vector3.Lerp(transform.position,
			   transform.position + pos, randomer2);
		break;
			case EnemyState.die:
				pos = new Vector3((Random.value - 0.5f) * randomer * speed * 100 + 1000,
		(Random.value - 0.5f) * randomer1 * speed * 100 + 1000, 0f);
		transform.position = Vector3.Lerp(transform.position,
			   transform.position + pos, randomer2);
				Destroy(this);
		break;

	}
		}
	

	private void OnTriggerEnter(Collider other)
	{
		triggered = true;
		if (this.state == EnemyState.norm)
		{
			if (other.gameObject.tag == "AttackCollider" && !(other.transform.parent.gameObject.GetComponent<FriendBehaviour>().state == FriendState.leave))
			{
				//TO theGM add points
				this.victim = other.transform.parent.gameObject;
				StartCoroutine(SetState(EnemyState.celebrate, 0.2f));
				StartCoroutine(SetState(EnemyState.attack, 2f));
			}
		
		}
		if (this.state == EnemyState.attack)
		{
			if (other.gameObject.tag == "Friend")
			{
				//TO theGM add points
				this.victim = other.transform.parent.gameObject;
				Destroy(other.gameObject);
				StartCoroutine(SetState(EnemyState.celebrate, 0f));
				StartCoroutine(SetState(EnemyState.leave, 1f));

			}
		}
	}

	private void OnTriggerStay(Collider other)
	{
		triggered = true;
		if (this.state == EnemyState.norm)
		{
			if (other.gameObject.tag == "AttackCollider")
			{
				//TO theGM add points
				StartCoroutine(SetState(EnemyState.celebrate, 0.2f));
				StartCoroutine(SetState(EnemyState.attack, 2f));
				this.victim = other.transform.parent.gameObject;
			}

		}
		if (this.state == EnemyState.attack)
		{
			if (other.gameObject.tag == "Friend")
			{
				//TO theGM add points
				this.victim = other.transform.parent.gameObject;
				Destroy(other.gameObject);
				StartCoroutine(SetState(EnemyState.celebrate, 0f));
				StartCoroutine(SetState(EnemyState.leave, 1f));

			}
		}
	}


	private void OnCollisionEnter(Collision other)
    {
		triggered = true;
		if (other.gameObject.tag == "Friend")
        {
			//TO theGM add points
			this.victim = other.transform.parent.gameObject;
			Destroy(other.gameObject); 
			StartCoroutine(SetState(EnemyState.celebrate, 0.2f));
			StartCoroutine(SetState(EnemyState.leave, 3f));
			
        }

		if (other.gameObject.tag == "AttackCollider")
		{
			this.victim = other.transform.parent.gameObject;
			StartCoroutine(SetState(EnemyState.celebrate, 0.2f));
			StartCoroutine(SetState(EnemyState.attack, 2f));
		}

	}
	IEnumerator SetState(EnemyState new_state, float wait_time)
	{
		yield return new WaitForSeconds(wait_time); //wait 10 seconds
		this.state = new_state;
	}
}