﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void LoadGame(){
		SceneManager.LoadScene ("gazegame_level1");
	}

	public void QuitGame(){
		Debug.Log ("You quit the game, theoretically...");
		Application.Quit ();
	}

	public void LoadMenu()
    {
		SceneManager.LoadScene("menu");
    }

	public void LoadCalibration()
    {
		SceneManager.LoadScene("calibration");
    }
}
