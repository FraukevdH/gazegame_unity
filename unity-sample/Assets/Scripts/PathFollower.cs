﻿using UnityEngine;
using System.Collections;

public class PathFollower : MonoBehaviour {

	Node[] PathNode;
	public float MoveSpeed = 0.2f;
	public GameObject Player;
	float Timer;
	int CurrentNode;
	static Vector3 CurrentPoistionHolder;
	static Vector3 StartPosition;
	// Use this for initialization
	void Start()
	{
		PathNode = this.GetComponentsInChildren<Node>();

		CurrentNode = 0;
		StartPosition = PathNode[0].transform.position;
		//Player = this.transform.parent.gameObject;

			CheckNode();

	}

	void CheckNode()
	{
		if (CurrentNode < PathNode.Length - 1)
		{ Timer = 0; }
		CurrentPoistionHolder = PathNode[CurrentNode].transform.position;
	}
	// Update is called once per frame
	void Update()
	{
		Timer *= MoveSpeed * Time.deltaTime;
		if (Player.transform.position != CurrentPoistionHolder)
		{
			Player.transform.position = Vector3.Lerp(StartPosition, CurrentPoistionHolder, Timer);
		}
		else
		{
			if (CurrentNode < (PathNode.Length - 1))
			{
				CurrentNode++;
				CheckNode();
			}
		}
	}
}
