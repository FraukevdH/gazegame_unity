﻿using UnityEngine;
using System.Collections;


public enum FriendState
{
	norm,
	leave,
	attack,
	celebrate,
	die
};

public class FriendBehaviour : MonoBehaviour {

	[SerializeField] public float speed = 0.05f;
	[SerializeField] public float randomer = 0.1f;
	[SerializeField] public float randomer1 = 0.015f;
	[SerializeField] public float randomer2 = 0.5f;
	[SerializeField] public float randomer3 = 0.02f;

	public GameObject[] goal_points;
	private GameObject goal_point;
	public FriendState state;
	private GameObject bowl;
	private GameObject[] bowls;
	// Use this for initialization
	void Start()
	{
		state = FriendState.norm;
		bowls = GameObject.FindGameObjectsWithTag("Bowl");
		goal_points = GameObject.FindGameObjectsWithTag("FriendGoalPoint");
		goal_point = goal_points[Random.Range(0, goal_points.Length)];

	}

	void FixedUpdate()
	{
		Vector3 pos = new Vector3();
		switch (state)
		{

			case FriendState.norm:
				pos = new Vector3((Random.value - 0.5f)+ transform.position.x,
				(Random.value - 0.5f)+ transform.position.y, transform.position.z);

				//transform.position = Vector3.Lerp(transform.position,
				//  goal_point.transform.position, randomer3);
				//transform.position = Vector3.Lerp(transform.position,
					//  transform.position + pos, randomer2);
				
				transform.position = Vector3.MoveTowards(transform.position, goal_point.transform.position, speed * Time.fixedDeltaTime);
				transform.position = Vector3.MoveTowards(transform.position, pos, speed  * Time.deltaTime);
				break;
			case FriendState.leave:
				pos = new Vector3((Random.value - 0.5f) + transform.position.x,
				(Random.value - 0.5f) + transform.position.y, transform.position.z);
				transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);

				transform.position = Vector3.MoveTowards(this.transform.position,
new Vector3(0, 0, transform.position.z), 0.003f);
				break;
			case FriendState.attack:
				transform.position = Vector3.MoveTowards(this.transform.position,
		  bowl.transform.position, 0.005f);

				break;
			case FriendState.celebrate:
				pos = new Vector3((Random.value - 0.5f) + transform.position.x,
							(Random.value - 0.5f) + transform.position.y, transform.position.z);
				transform.position = Vector3.MoveTowards(transform.position, pos,4* speed * Time.deltaTime);

				break;
			case FriendState.die:
				pos = new Vector3((Random.value - 0.5f) * randomer * speed,
		(Random.value - 0.5f) * randomer1 * speed, 0f);
				transform.position = Vector3.Lerp(transform.position,
					   transform.position + pos, randomer2);
				break;

		}
	}


	private void OnTriggerEnter(Collider other)
	{
		if (other.transform.gameObject.tag == "Friend")
			return;
		if (this.state == FriendState.norm)
		{


			if (other.transform.gameObject.tag == "Bowl")
			{
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;
				//TO theGM add points


				if (b_state == BowlState.full)
				{



					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.leave, 1f));
					bowl.GetComponent<BowlBehaviour>().Empty_Bowl();
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}

				if (other.transform.gameObject.tag == "BowlAttackCollider")
				{
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;

				if (b_state == BowlState.full)
					{


						StartCoroutine(SetState(FriendState.celebrate, 0.5f));
						StartCoroutine(SetState(FriendState.attack, 2f));
					}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}
			}
		
		if (this.state == FriendState.attack)
		{

			if (other.transform.gameObject.tag == "Bowl")
			{
				this.bowl = other.transform.gameObject;
				BowlState b_state = this.bowl.transform.GetComponent<BowlBehaviour>().state;
				//TO theGM add points


				if (b_state == BowlState.full)
				{



					StartCoroutine(SetState(FriendState.celebrate, 0.5f));
					StartCoroutine(SetState(FriendState.leave, 1f));
					bowl.GetComponent<BowlBehaviour>().Empty_Bowl();
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}

			}
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (this.state == FriendState.norm)
		{
		
			if (other.gameObject.tag == "Bowl")
			{
				//TO theGM add points
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;

				if (b_state == BowlState.full)
				{
					bowl.GetComponent<BowlBehaviour>().Empty_Bowl();
					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.leave, 1f));
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}

			if (other.gameObject.tag == "BowlAttackCollider")
			{
				this.bowl = other.transform.parent.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;

				if (b_state == BowlState.full)
				{
					this.bowl.GetComponent<BowlBehaviour>().Empty_Bowl();

					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.attack, 2f));
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}
		}
	}

	private void OnTriggerEnter(Collision other)
	{
		if (this.state == FriendState.norm)
		{


			if (other.gameObject.tag == "Bowl")
			{
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;

				if (b_state == BowlState.full)
				{
					bowl.GetComponent<BowlBehaviour>().Empty_Bowl();
					//TO theGM add points
					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.leave, 1f));
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}

			if (other.gameObject.tag == "BowlAttackCollider")
			{
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;
				if (b_state == BowlState.full)
				{
					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.attack, 2.5f));

				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if (this.state == FriendState.norm)
		{
			
			
			if (other.gameObject.tag == "Bowl")
			{
				//TO theGM add points
				this.bowl = other.transform.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;
				if (b_state == BowlState.full)
				{
				
					StartCoroutine(SetState(FriendState.celebrate, 0.2f));
					StartCoroutine(SetState(FriendState.leave, 1f));
					this.bowl.GetComponent<BowlBehaviour>().Empty_Bowl();
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}

			}

			if (other.gameObject.tag == "BowlAttackCollider")
			{
				this.bowl = other.transform.parent.gameObject;
				BowlState b_state = this.bowl.GetComponent<BowlBehaviour>().state;
				if (b_state == BowlState.full)
				{

					StartCoroutine(SetState(FriendState.celebrate, 0f));
					StartCoroutine(SetState(FriendState.attack, 0.2f));
				}
				if (b_state == BowlState.empty)
				{
					StartCoroutine(SetState(FriendState.norm, 0.05f));
				}
			}
		}
	}
	IEnumerator SetState(FriendState new_state, float wait_time)
	{
		yield return new WaitForSeconds(wait_time); //wait 10 seconds
		if (new_state == FriendState.attack)
        {
			this.state = FriendState.attack;

		}
		if (new_state == FriendState.celebrate)
		{
			this.state = FriendState.celebrate;
					}
		if (new_state == FriendState.norm)
		{
			this.state = FriendState.norm;
		}
		if (new_state == FriendState.leave)
		{
			this.state = FriendState.leave;
		}
	}
}