﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EyeTribe.ClientSdk;
using EyeTribe.ClientSdk.Data;
using EyeTribe.Unity;

public class EyeTribeManager : MonoBehaviour {

	public void ToggleIndicator()
    {
        GazeGUIController.ToggleIndicatorMode();
    }

    public void ToggleSmooth()
    {
        GazeGUIController.ToggleSmoothMode();
    }

    public void ToggleInfo()
    {
        GazeGUIController.ToggleDebugMode();
    }

}
